﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiquidHazardController : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D otherCollider) {
        if (IsPlayer(otherCollider)) {
            otherCollider.GetComponent<Player>().Hit();
        }
    }

    bool IsPlayer(Collider2D collider) {
        return collider.CompareTag("Player");
    }
}
