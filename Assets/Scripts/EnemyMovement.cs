﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    [SerializeField] float moveSpeed = 1f;

    Rigidbody2D myRigidbody;
    bool touchingGround = false;

    void Start() {
        myRigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        if (touchingGround) {
            myRigidbody.velocity = new Vector2(moveSpeed, 0f);
        } else {
            myRigidbody.velocity = new Vector2(0, myRigidbody.velocity.y);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        touchingGround = true;
    }


}
