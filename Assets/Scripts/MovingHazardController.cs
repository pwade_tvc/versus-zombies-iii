﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingHazardController : MonoBehaviour {

    [Header("Config")]
    [SerializeField] float speed = 1f;
    [SerializeField] float nodePauseInSecs = 0;
    [SerializeField] [Range(0, 2)] float easeAmount = 0;
    [SerializeField] bool isCyclic = false;
    [SerializeField] Vector3[] localWaypoints = default;
    [SerializeField] public bool useRigidbody = false;

    Vector3[] globalWaypoints;
    int fromWaypointIndex;
    float percentMoved;
    float nextMoveTime;

    Rigidbody2D myRigidbody;
    Animator animator;

    void Start() {
        globalWaypoints = new Vector3[localWaypoints.Length];
        for (int i = 0; i < localWaypoints.Length; i++) {
            globalWaypoints[i] = localWaypoints[i] + transform.position;
        }

        // Only used when useRigidboy = true
        myRigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void Update() {
        if (!useRigidbody) {
            Vector3 velocity = CalculateMovement();
            transform.Translate(velocity);
        }
    }

    void FixedUpdate() {
        if (useRigidbody) {
            Vector3 velocity = CalculateMovement();
            myRigidbody.MovePosition(myRigidbody.position + new Vector2(velocity.x, velocity.y));
        }
    }

    private void OnTriggerEnter2D(Collider2D otherCollider) {
        if (IsPlayer(otherCollider)) {
            otherCollider.GetComponent<Player>().Hit();
        }
    }

    bool IsPlayer(Collider2D otherCollider) {
        return otherCollider.CompareTag("Player");
    }

    Vector3 CalculateMovement() {

        if (Time.time < nextMoveTime) {
            return Vector3.zero;
        }

        fromWaypointIndex %= globalWaypoints.Length;
        int toWaypointIndex = (fromWaypointIndex + 1) % globalWaypoints.Length;

        float distanceBetweenWaypoints = Vector3.Distance(globalWaypoints[fromWaypointIndex], globalWaypoints[toWaypointIndex]);
        percentMoved += Time.deltaTime * speed / distanceBetweenWaypoints;
        percentMoved = Mathf.Clamp01(percentMoved);

        float easedPercentMoved = Ease(percentMoved);

        Vector3 newPos = Vector3.Lerp(globalWaypoints[fromWaypointIndex], globalWaypoints[toWaypointIndex], easedPercentMoved);

        if (percentMoved >= 1) {
            percentMoved = 0;
            fromWaypointIndex++;

            if (!isCyclic) {
                if (fromWaypointIndex >= globalWaypoints.Length - 1) {
                    fromWaypointIndex = 0;
                    System.Array.Reverse(globalWaypoints);
                }
            }

            nextMoveTime = Time.time + nodePauseInSecs;
        }

        return newPos - transform.position;
    }

    /* Easing formula:
     * 
     *           x^a
     * y = ---------------
     *     x^a + (1 - x)^a
     *     
     * When a=1, no easing is performed.
     */
    float Ease(float x) {
        float a = easeAmount + 1;
        float _xa = Mathf.Pow(x, a);
        float _1xa = Mathf.Pow((1 - x), a);
        return _xa / (_xa + _1xa);
    }

    private void OnDrawGizmos() {
        if (localWaypoints != null) {
            Gizmos.color = Color.cyan;
            float size = .3f;
            for (int i = 0; i < localWaypoints.Length; i++) {
                Vector3 globalWaypointPos = (Application.isPlaying) ? globalWaypoints[i] : localWaypoints[i] + transform.position;
                Gizmos.DrawLine(globalWaypointPos - Vector3.up * size, globalWaypointPos + Vector3.up * size);
                Gizmos.DrawLine(globalWaypointPos - Vector3.left * size, globalWaypointPos + Vector3.left * size);
            }
        }
    }
}
