﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelExit : MonoBehaviour {

    [SerializeField] float levelLoadDelay = 1f;

    Player player;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (IsPlayer(collision)) {
            player = collision.GetComponent<Player>();
            player.IsAlive = false;
            GetComponent<Animator>().SetTrigger("triggerOpen");
        }
    }

    public void LoadNextLevel() {
        StartCoroutine(LoadAfterDelay());
        player.FadeOut();
    }

    private IEnumerator LoadAfterDelay() {
        yield return new WaitForSecondsRealtime(levelLoadDelay);
        var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }

    bool IsPlayer(Collider2D collider) {
        return collider.CompareTag("Player");
    }
}
