﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(Controller2D))]
public class Player : MonoBehaviour {

    [Header("Config")]
    [SerializeField] float moveSpeed = 6;
    [SerializeField] float jumpHeight = 4;
    [SerializeField] float accelerationTimeAirborne = 0.2f;
    [SerializeField] float accelerationTimeGrounded = 0.1f;
    [SerializeField] float timeToJumpApex = 0.4f;
    [SerializeField] float deathKickAmount = 2;

    [Header("Debug")]
    [SerializeField] float jumpVelocity;
    [SerializeField] float gravity;
    [SerializeField] float velocitySmoothingX;

    Vector3 velocity;
    public bool IsAlive { get; set; } = true;

    Controller2D controller;
    Animator animator;

    void Start() {
        controller = GetComponent<Controller2D>();
        animator = GetComponent<Animator>();
        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
    }

    private void Update() {
        Move();
    }

    void FixedUpdate() {
        if (controller.useRigidbody) {
            var adjustedVelocity = velocity * Time.deltaTime;
            controller.Move(adjustedVelocity);
        }
    }

    public void Hit() {
        if (IsAlive) {
            print("I've been hit, ouch");
            Die();
        }
    }

    private void Die() {
        IsAlive = false;
        animator.SetTrigger("triggerDie");
        velocity.y = jumpVelocity / 2;
        if (GetComponentInChildren<SpriteRenderer>().flipX) {
            velocity.x = deathKickAmount;
        } else {
            velocity.x = -deathKickAmount;
        }
        controller.Move(velocity * Time.deltaTime);
    }

    public void FadeOut() {
        animator.SetTrigger("triggerFadeOut");
    }

    void Move() {
        if (controller.collisions.above || controller.collisions.below) {
            velocity.y = 0;
        }

        if (IsAlive) {
            Vector2 input = new Vector2(CrossPlatformInputManager.GetAxisRaw("Horizontal"), CrossPlatformInputManager.GetAxisRaw("Vertical"));
            if (IsInputDetected(input.x)) {
                FlipSprite();
                animator.SetBool("isRunning", true);
            } else {
                if (animator.GetBool("isRunning") == true) {
                    animator.SetBool("isRunning", false);
                }
            }
            if (CrossPlatformInputManager.GetButtonDown("Jump") && controller.collisions.below) {
                velocity.y = jumpVelocity;
                animator.SetTrigger("triggerJump");
            }
            float targetVelocityX = input.x * moveSpeed;
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocitySmoothingX, (controller.collisions.below ? accelerationTimeGrounded : accelerationTimeAirborne));
        } else {
            // If we're dead, keep x velocity only until we've hit the ground cause it looks cooler when we die
            if (controller.collisions.below) {
                velocity.x = 0;
            }
            // And we can't be running
            animator.SetBool("isRunning", false);
        }

        velocity.y += gravity * Time.deltaTime;

        if (!controller.useRigidbody) {
            var adjustedVelocity = velocity * Time.deltaTime;
            controller.Move(adjustedVelocity);
        }
        
    }

    private void FlipSprite() {
        // If the x velocity is negative we need to flip the sprite
        bool flipX = (Mathf.Sign(velocity.x) == -1);
        GetComponentInChildren<SpriteRenderer>().flipX = flipX;
    }
    
    private bool IsInputDetected(float inputAmount) {
        return Mathf.Abs(inputAmount) > Mathf.Epsilon;
    }
}
